**UniPac** : Gestionnaire universel par paquets.

Ce gestionnaire de paquets permet de produire ou compléter à volonté des architectures de fichiers.

# Utilisation
Le dossier d’utilisation est appelé dossier racine du projet.
Il sauvegarde les données des paquets déployés et sert de référence pour que les fichiers soient au même niveau dans le système de fichier.

# Paquet
Un paquet est une archive contenant des fichiers inter-dépendants et cohérents.
Il peut s’agir de programmes, de fichiers de configurations, d’extensions, de ressources.

Un paquet est identifié de manière unique par son **id** (différent de son nom commun) et sa **version**.
Son contenu est parfois limité à certains supports matériels (CPU x86_64, risc-V, arm) et certains systèmes (Linux, Windows, MacOS) ou être universel comme le code Python et le _bytecode_ Java.

Les relations avec les autres paquets sont :
- la _dépendance_ quand le contenu nécessite la présence d’un autre paquet pour fonctionner.
- la _dépendance optionnelle_ pour suggérer l’ajout d’extensions.
- le _conflit_ empêche l’utilisation simultanée de deux paquets.
- la _fourniture_ décrit des sous-composants du paquet.

Les autres informations sont destinées à garantir son intégrité ou renseigner l’utilisateur.

## Version
Une version doit respecter [la sémantique](https://semver.org/) `MAJOR.MINOR.FIX[-release][+build]`.
Un intervalle de version contient optionnellement une version minimum et maximum et indique si les bornes sont inclusives ou exclusives.
_ex: ">1.19,<1.20" ou "[1.19,1.20)"_
