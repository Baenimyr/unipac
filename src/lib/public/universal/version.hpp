/**
 * \file version.hpp
 * \author Benjamin Le Rohellec
 * \brief Ce fichier définit la structure d’une version et d’un intervalle de version selon le standard.
 * \version 0.1
 * \date 2023-04-14
 *
 * Ce fichier est extrait du projet Universal.
 * \see https://semver.org/lang/fr/spec/v2.0.0.html
 *
 **/
#pragma once
#include "unipac/api.h"
#include <compare> // std::strong_ordering
#include <functional>
#include <optional>
#include <ostream>
#include <string>
#include <string_view>
#include <sys/types.h>

namespace universal {

/**
 * \brief Structure d’une version
 *
 * Une version contient jusqu'à 3 niveaux: major, minor, et patch.
 * \see https://semver.org/lang/fr/spec/v2.0.0.html
 */
class UNIPAC_API Version {
public:
	using version_t = unsigned int;
	version_t major, minor, patch;
	std::optional<std::string> release, build;

	constexpr Version(version_t major, version_t minor = 0, version_t patch = 0) noexcept :
		major(major),
		minor(minor),
		patch(patch) {
	}

	constexpr Version(Version const &) = default;
	constexpr Version(Version &&) = default;

	Version & operator=(Version const &) = default;
	Version & operator=(Version &&) = default;

	/** \brief Égalité entre deux versions.
	 *
	 * Les versions sont égales si les sous-versions sont identiques mais aussi le <i>release</i> et le <i>build</i>. */
	constexpr bool operator==(Version const &) const noexcept = default;

	/** \brief Compare les versions.
	 *
	 * Une version sans <i>release</i> est supérieure à une version avec <i>release</i>.
	 * Même chose pour le <i>build</i>. */
	std::strong_ordering operator<=>(Version const &) const noexcept;
	operator std::string() const noexcept;
}; //// class Version

static_assert(std::is_copy_constructible<Version>::value);
static_assert(std::is_move_constructible<Version>::value);
static_assert(std::is_copy_assignable<Version>::value);
static_assert(std::is_move_assignable<Version>::value);

UNIPAC_API std::ostream & operator<<(std::ostream &, Version const &);

UNIPAC_API Version to_version(std::string const & sw);
UNIPAC_API Version to_version(std::istream &);

/**
 * \brief Une intervalle de version.
 *
 * Une intervalle définit un ensemble continu de version.
 * Elle peut utiliser une version minimum et une version maximum.
 * Si la borne n'est pas définit, l'intervalle est ouverte de ce côté.
 */
class UNIPAC_API VersionIntervalle {
public:
	std::optional<Version> minimum;
	std::optional<Version> maximum;
	bool inclut_min;
	bool inclut_max;

	/** \brief Crée une intervalle de version.
	 *
	 * \param minimum version minimum délimitant l’intervalle
	 * \param maximim version maximum délimitant l’intervalle
	 * \param inclut_min si la version minimale est comprise dans l’intervalle ou juste en dehors
	 * \param inclut_min si la version maximum est comprise dans l’intervalle ou juste en dehors
	 */
	VersionIntervalle(std::optional<Version> const & minimum, std::optional<Version> const & maximum, bool inclut_min,
		bool inclut_max) noexcept;

	/** \see version_intervalle(std::optional<version> const&, std::optional<version> const&, bool, bool) */
	VersionIntervalle(std::optional<Version> && minimum, std::optional<Version> && maximum, bool inclut_min,
		bool inclut_max) noexcept;

	VersionIntervalle(VersionIntervalle const &) = default;
	VersionIntervalle(VersionIntervalle &&) = default;

	static VersionIntervalle all() {
		return {std::nullopt, std::nullopt, false, false};
	}

	static VersionIntervalle unique(Version const & v) {
		return {v, v, true, true};
	}

	bool operator==(VersionIntervalle const &) const noexcept = default;

	/** \brief Compare les intervalles.
	 *
	 * Deux intervalles sont d'abord comparées par leurs bornes minimums puis leurs bornes maximales.
	 * Les intervalles sont égales si les bornes sont identiques et que les inclusions ( \link ::inclut_min \endlink )
	 * sont identiques.
	 */
	std::strong_ordering operator<=>(VersionIntervalle const &) const noexcept;

	/** \brief Compare la position de la version dans l'intervalle.
	 *
	 * Une version est comprise dans l'intervalle si l'intervalle est ouverte et
	 * que la version est supérieure, respectivement inférieure à la borne inférieure, respectivement supérieure.
	 */
	constexpr bool contains(Version const &) const noexcept;
	operator std::string() const noexcept;

	VersionIntervalle & operator=(VersionIntervalle const &) = default;
	VersionIntervalle & operator=(VersionIntervalle &&) = default;

	/** \brief Intersection entre deux versions. */
	VersionIntervalle & operator&=(VersionIntervalle const &);
}; //// version_intervalle

static_assert(std::is_copy_constructible<VersionIntervalle>::value);
static_assert(std::is_move_constructible<VersionIntervalle>::value);
static_assert(std::is_copy_assignable<VersionIntervalle>::value);
static_assert(std::is_move_assignable<VersionIntervalle>::value);

UNIPAC_API std::ostream & operator<<(std::ostream &, VersionIntervalle const &);

/** \brief Permet de comparer deux bornes d'intervalle.
 *
 * Deux bornes sont égales si elles sont toutes les deux absentes ou de même valeur.
 * Une borne absente est considérée à l'infini, inférieur pour v1, supérieur pour v2.
 * \param v1 borne inférieure ou -inf
 * \param v2 borne supérieure ou +inf
 */
constexpr std::strong_ordering operator<=>(
	std::optional<Version> const & v1, std::optional<Version> const & v2) noexcept;

UNIPAC_API VersionIntervalle to_version_intervalle(std::string_view);

} // namespace universal

//
// ---------- hash ----------
//
namespace std {
template<> struct UNIPAC_API hash<universal::Version> {
	static constexpr std::hash<std::string> hash_string {};
	std::size_t operator()(universal::Version const &) const noexcept;
}; //// hash<universal::Version>

template<> struct UNIPAC_API hash<std::optional<universal::Version>> {
	static constexpr std::hash<universal::Version> hash_version {};
	std::size_t operator()(std::optional<universal::Version> const &) const noexcept;
}; //// hash<std::optional<universal::Version>>

template<> struct UNIPAC_API hash<universal::VersionIntervalle> {
	static constexpr std::hash<std::optional<universal::Version>> hash_version {};
	std::size_t operator()(universal::VersionIntervalle const &) const noexcept;
}; //// hash<universal::VersionIntervalle>
} // namespace std

// ------------------------------ version ------------------------------
namespace universal {
constexpr std::strong_ordering operator<=>(
	std::optional<Version> const & v1, std::optional<Version> const & v2) noexcept {
	if(v1 && v2)
		return v1.value() <=> v2.value();
	else if(v1 && !v2)
		return std::strong_ordering::greater;
	else if(!v1 && v2)
		return std::strong_ordering::less;
	else
		return std::strong_ordering::equal;
}

// ------------------------------ version_intervalle ------------------------------
constexpr bool VersionIntervalle::contains(Version const & v) const noexcept {
	return (!minimum.has_value() || (inclut_min ? minimum.value() <= v : minimum.value() < v)) &&
		   (!maximum.has_value() || (inclut_max ? v <= maximum.value() : v < maximum.value()));
}
} // namespace universal
