/**
 * @file paquet.hpp
 * @author Benjamin Le Rohellec
 * @brief Description d’un paquet.
 *
 * Le système de paquet est inspiré de libalpm.
 * @see https://archlinux.org/pacman/
 **/
#pragma once
#include "api.h" //UNIPAC_API
#include "fichier.hpp"
#include "universal/version.hpp"
#include <compare> //std::strong_ordering
#include <set>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

namespace unipac {

/** @brief Type pour les identifiants de paquet. */
using paquet_id_t = std::string;
/** @brief Type pour les différentes architectures de CPU. */
using arch_t = std::string;
/** @brief Type pour distinguer les systèmes d’exploitation. */
using os_t = std::string;
constexpr std::string_view arch_any("any");
constexpr std::string_view os_any("any");

/**
 * @brief Déclaration d’une dépendance entre paquets.
 *
 * Une dépendance identifie un autre paquet par son nom et indique l’intervalle de versions concernée.
 * La dépendance peut aussi être utilisée pour déclarer un conflit entre paquets.
 */
struct UNIPAC_API Dependance {
	/** @brief Nom du paquet ciblé. */
	paquet_id_t nom;
	/** @brief Intervalle de versions du paquet ciblé. */
	universal::VersionIntervalle version;
	/** @brief Description optionnelle de la raison de la relation. */
	std::string desc;

public:
	Dependance(paquet_id_t, universal::VersionIntervalle = universal::VersionIntervalle::all(), std::string = "");

	std::strong_ordering operator<=>(Dependance const &) const noexcept;
}; //// Dependance

using DependancesSet = std::set<Dependance>;

class UNIPAC_API Paquet {
private:
	paquet_id_t _id;
	universal::Version _version;

public:
	/** @brief Système d’exploitation compatible.
	 *
	 * La valeur \e any indique que le paquet est universellement compatible.
	 */
	os_t os = os_t(os_any);

	/** @brief Architecture du processeur.
	 *
	 * L’architecture du processeur peut faire varier les composants compilés.
	 * Peut être par exemple « any », « amd64 », « arm64 », « i386 ».
	 * L’architecture \e any indique que le paquet est universellement compatible.
	 **/
	arch_t arch = arch_t(arch_any);

	/** @brief Description du paquet. */
	std::string desc;
	/** @brief Site internet du projet ou du code. */
	std::string url;

	// ----- Relations -----
	/** @brief Liste des dépendances. */
	DependancesSet dependances;
	/** @brief Liste des dépendances optionnelles. */
	DependancesSet opt_depends;
	/** @brief Liste des conflits.
	 *
	 * Deux paquets en conflits ne peuvent pas être installés en même temps.
	 */
	DependancesSet conflits;
	/** @brief Liste d’alias et de composants fournis par ce paquet. */
	DependancesSet provisions;

	// ----- Fichiers -----
	/** @brief Tous les fichiers contenus dans ce paquet. */
	std::vector<Fichier> fichiers;

public:
	Paquet(Paquet &&) noexcept;
	Paquet(Paquet const &);
	Paquet(paquet_id_t pid, universal::Version version);
	~Paquet() noexcept = default;

	Paquet & operator=(Paquet &&) noexcept;
	Paquet & operator=(Paquet const &);

public:
	auto const & id() const noexcept {
		return _id;
	}

	auto const & version() const noexcept {
		return _version;
	}

	bool operator==(Paquet const &) const noexcept;
	/** @brief Compare les paquets par leur id et version. */
	std::strong_ordering operator<=>(Paquet const &) const noexcept;
	std::strong_ordering operator<=>(std::tuple<paquet_id_t, universal::Version> const &) const noexcept;
}; //// Paquet

} // namespace unipac
