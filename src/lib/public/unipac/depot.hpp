/**
 * @file depot.hpp
 * @brief Gestion d’un dépôt de paquets.
 * @version 0.1
 * @date 2023-04-13
 *
 * Les paquets et toutes leurs versions sont entreposés sur un serveur distant.
 * Ce serveur fournis un résumé des paquets disponibles et des données associées afin que le client puisse établir une
 * solution aux demandes utilisateurs. Le données descriptives sont manipulées par la classe Paquet, et les paquet sont
 * rassemblées dans la classe Dépôt.
 *
 * La classe Dépôt est un conteneur à paquets.
 */
#pragma once

#include "api.h"
#include "paquet.hpp"
#include "unipac/paquet.hpp"
#include "universal/version.hpp"
#include <unordered_map>
#include <vector>

namespace unipac {

/**
 * @brief Lieu d’entreposage des paquets.
 *
 * Pour en faciliter l’accès, les paquets sont regroupés par identifiant et il est possible d’itérer à travers les
 *paquets ayant un même identifiant.
 **/
class UNIPAC_API Depot {
public:
	using paquet_t = Paquet;
	/** @brief Groupe de paquet d’un même id. */
	using paquets = std::vector<paquet_t>;
	using paquets_map = std::unordered_map<paquet_id_t, paquets>;
	using size_t = std::size_t;

	/** @brief Itérateur sur les groupes de paquets. */
	using iterator = paquets_map::iterator;
	using const_iterator = paquets_map::const_iterator;
	/** @brief Itérateur sur les paquets d’un même groupe. */
	using groupe_iterator = paquets::iterator;
	using groupe_const_iterator = paquets::const_iterator;

private:
	paquets_map _paquets;

public:
	virtual ~Depot() noexcept = default;
	Depot() = default;
	Depot(Depot &&) noexcept = default;
	Depot(Depot const &) = default;

	Depot & operator=(Depot &&) noexcept = default;
	Depot & operator=(Depot const &) = default;

	// -- Lecture --
	/**
	 * @return true Si aucun paquet n’est enregistré dans ce dépôt.
	 **/
	[[nodiscard]] bool empty() const noexcept;
	[[nodiscard]] size_t size() const noexcept;
	/**
	 * @return true Si au moins un paquet correspond à l’identifiant.
	 **/
	[[nodiscard]] bool contient(paquet_id_t const & pid) const;
	/**
	 * @return true Si ce dépôt contient un paquet avec le même identifiant et exactement la même version.
	 **/
	[[nodiscard]] bool contient(paquet_id_t const & pid, universal::Version const &) const;

	const_iterator begin() const;
	const_iterator end() const;
	groupe_const_iterator begin(paquet_id_t const &) const;
	groupe_const_iterator end(paquet_id_t const &) const;

	/**
	 * @brief Accède au paquet correspondant à l’identifiant et la version.
	 *
	 * @throw std::invalid_argument si un tel paquet n’existe pas.
	 **/
	Paquet const & paquet(paquet_id_t const &, universal::Version const &) const;
	Paquet & paquet(paquet_id_t const &, universal::Version const &);

	// -- Modification --
	/**
	 * @brief Ajoute ou replace un paquet enregistré
	 *
	 * @return true si le paquet a été ajouté.
	 * @return false si un même paquet est déjà présent.
	 */
	virtual bool ajout(paquet_t);

	/**
	 * @brief Retire un paquet du dépôt.
	 *
	 * @return false si aucun paquet ne correspondait à ce nom et à cette version.
	 */
	virtual bool suppression(paquet_id_t const &, universal::Version const &);

	/**
	 * @brief Supprime tous les paquets correspondant au même identifiant.
	 */
	virtual bool suppression(paquet_id_t const &);
}; //// Depot

} // namespace unipac
