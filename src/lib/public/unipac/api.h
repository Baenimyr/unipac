#ifndef UNIPAC_EXPORT
#if UNIPAC_STATIC
    #define UNIPAC_EXPORT
    #define UNIPAC_IMPORT
    #define UNIPAC_NO_EXPORT
#elif __linux__
    #define UNIPAC_EXPORT __attribute__((visibility("default")))
    #define UNIPAC_IMPORT __attribute__((visibility("default")))
    #define UNIPAC_NO_EXPORT __attribute__((visibility("hidden")))
#elif _WIN32
    #define UNIPAC_EXPORT __declspec(dllexport)
    #define UNIPAC_IMPORT __declspec(dllimport)
    #define UNIPAC_NO_EXPORT
#else
    #error "Plateforme inconnue."
#endif

#if unipac_EXPORTS
    #define UNIPAC_API UNIPAC_EXPORT
#else
    #define UNIPAC_API UNIPAC_IMPORT
#endif
#endif
