/**
 * @file fichier.hpp
 * @author Benjamin Le Rohellec
 * @brief Gère tous les aspects des fichiers.
 */
#pragma once

#include "api.h" // UNIPAC_API
#include <compare> // std::strong_ordering
#include <cstdint> // std::uintmax_t
#include <filesystem> // std::filesystem::path

namespace unipac {

/**
 * @brief Informations d’un fichier, sauvegardés dans la déclaration du paquet.
 *
 * L’emplacement du fichier est absolu à partir du dossier racine.
 * Il est impossible de positionner un fichier en dehors du dossier de déploiement.
 *
 * Cette structure enregistre le nécessaire pour garantir l’intégrité du fichier déployé.
 */
struct UNIPAC_API Fichier {
	std::filesystem::path nom;
	/** @brief Taille du fichier, en octets. */
	std::uintmax_t taille;
	// TODO sha

public:
	Fichier(Fichier &&) noexcept = default;
	Fichier(Fichier const &) = default;
	~Fichier() noexcept = default;
	Fichier(std::filesystem::path const &, std::uintmax_t);

	Fichier & operator=(Fichier &&) noexcept = default;
	Fichier & operator=(Fichier const &) = default;

public:
	/**
	 * @brief Compare les fichiers selon leur nom.
	 **/
	std::strong_ordering operator<=>(Fichier const &) const noexcept;

	/**
	 * @brief Teste si le fichier a été modifié.
	 *
	 * @param root dossier de déploiement.
	 **/
	bool est_modifie(std::filesystem::path const & root) const;
}; //// Fichier

/**
 * @brief Préserve le fichier.
 *
 * Préserve un fichier qui a été modifié par l’utilisateur, mais le rend inopérant.
 * @return false en cas de conflit entre le fichier de sauvegarde et un fichier déjà présent.
 */
bool UNIPAC_API backup(std::filesystem::path const &);

/**
 * @brief Restaure un fichier.
 *
 * Rétablit un fichier préalablement préservé.
 *
 * @return false si le fichier est déjà là.
 */
bool UNIPAC_API restore(std::filesystem::path const &);

} // namespace unipac
