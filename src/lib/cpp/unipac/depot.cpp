#include "unipac/depot.hpp"
#include "unipac/paquet.hpp"
#include "universal/version.hpp"
#include <algorithm>
#include <stdexcept>
#include <tuple>

namespace unipac {

bool Depot::empty() const noexcept {
	return this->_paquets.empty();
} //// Depot::empty()

Depot::size_t Depot::size() const noexcept {
	size_t t = 0;
	for(auto const & g : this->_paquets) {
		t += g.second.size();
	}
	return t;
} //// Depot::size()

bool Depot::contient(paquet_id_t const & _id) const {
	return this->_paquets.contains(_id);
} //// Depot::contient(paquet_id_t)

bool Depot::contient(paquet_id_t const & _id, universal::Version const & version) const {
	if(!this->contient(_id))
		return false;
	auto const & groupe = this->_paquets.at(_id);
	return std::binary_search(groupe.begin(), groupe.end(), std::tuple<paquet_id_t, universal::Version>(_id, version));
} //// Depot::contient(paquet_id_t, Version)

Depot::const_iterator Depot::begin() const {
	return this->_paquets.begin();
} //// Depot::begin()

Depot::const_iterator Depot::end() const {
	return this->_paquets.end();
} //// Depot::begin()

Depot::groupe_const_iterator Depot::begin(paquet_id_t const & _id) const {
	return this->_paquets.at(_id).begin();
} //// Depot::begin(paquet_id_t)

Depot::groupe_const_iterator Depot::end(paquet_id_t const & _id) const {
	return this->_paquets.at(_id).end();
} //// Depot::end(paquet_id_t)

bool Depot::ajout(paquet_t p) {
	if(!this->contient(p.id()))
		this->_paquets.emplace(p.id(), paquets{});
	auto & groupe = this->_paquets.at(p.id());
	auto emplace = std::lower_bound(groupe.begin(), groupe.end(), p);
	if(emplace != groupe.end() && *emplace == p)
		return false;
	groupe.emplace(emplace, std::move(p));
	return true;
} //// Depot::ajout

bool Depot::suppression(paquet_id_t const & pid, universal::Version const & version) {
	if(!this->contient(pid))
		return false;
	auto & groupe = this->_paquets.at(pid);
	auto iter =
		std::lower_bound(groupe.begin(), groupe.end(), std::tuple<paquet_id_t, universal::Version>(pid, version));
	if(iter != groupe.end()) {
		groupe.erase(iter);
	} else {
		return false;
	}
	if(groupe.empty()) {
		this->suppression(pid);
	}
	return true;
} //// Depot::suppression

bool Depot::suppression(paquet_id_t const & pid) {
	return this->_paquets.erase(pid) != 0;
} //// Depot::suppression

Paquet & Depot::paquet(paquet_id_t const & pid, universal::Version const & version) {
	auto & groupe = this->_paquets.at(pid);
	auto const tuple_id = std::make_tuple(pid, version);
	auto iter = std::lower_bound(groupe.begin(), groupe.end(), tuple_id);
	if(iter != groupe.end() && iter->id() == pid && iter->version() == version)
		return *iter;
	throw std::invalid_argument(pid + "=" + std::string(version));
} //// Depot::paquet

Paquet const & Depot::paquet(paquet_id_t const & pid, universal::Version const & version) const {
	auto & groupe = this->_paquets.at(pid);
	auto const tuple_id = std::make_tuple(pid, version);
	auto iter = std::lower_bound(groupe.begin(), groupe.end(), tuple_id);
	if(iter != groupe.end() && iter->id() == pid && iter->version() == version)
		return *iter;
	throw std::invalid_argument(pid + "=" + std::string(version));
} //// Depot::paquet

} // namespace unipac
