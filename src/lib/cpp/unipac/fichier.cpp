#include "unipac/fichier.hpp"
#include "unipac/api.h"
#include <compare> // std::strong_ordering
#include <filesystem> // std::filesystem::exists

namespace unipac {

Fichier::Fichier(std::filesystem::path const & nom, std::uintmax_t taille) : nom(nom.relative_path()), taille(taille) {
}

std::strong_ordering Fichier::operator<=>(Fichier const & f) const noexcept {
	return this->nom <=> f.nom;
}

bool Fichier::est_modifie(std::filesystem::path const & root) const {
	auto const abs = root / this->nom;
	if(!std::filesystem::exists(abs))
		return true;
	if(std::filesystem::file_size(abs) != this->taille)
		return true;
	return false;
} //// Fichier::est_modifie

auto UNIPAC_NO_EXPORT nom_sauvegarde(std::filesystem::path const & n) {
	auto sauv = n;
	sauv += ".save";
	return sauv;
} //// nom_sauvegarde

bool backup(std::filesystem::path const & f) {
	auto const sav = nom_sauvegarde(f);
	if(std::filesystem::exists(sav)) {
		return false;
	}
	std::filesystem::rename(f, sav);
	return true;
} //// backup

bool restore(std::filesystem::path const & f) {
	auto const fichier_sauvegarde = nom_sauvegarde(f);
	if(std::filesystem::exists(fichier_sauvegarde)) {
		if(std::filesystem::exists(f))
			return false;
		std::filesystem::rename(fichier_sauvegarde, f);
	}
	return true;
} //// restore

} // namespace unipac
