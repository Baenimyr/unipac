#include "unipac/paquet.hpp"
#include "universal/version.hpp"
#include <compare>
#include <string>
#include <utility> // std::move

unipac::Dependance::Dependance(unipac::paquet_id_t pid, universal::VersionIntervalle versions, std::string desc) :
	nom(std::move(pid)),
	version(std::move(versions)),
	desc(std::move(desc)) {
}

std::strong_ordering unipac::Dependance::operator<=>(unipac::Dependance const & dep) const noexcept {
	auto cmp_nom = this->nom <=> dep.nom;
	if(cmp_nom == std::strong_ordering::equal) {
		return this->version <=> dep.version;
	}
	return cmp_nom;
} //// Dependance::operator<=>

unipac::Paquet::Paquet(unipac::Paquet &&) noexcept = default;

unipac::Paquet::Paquet(unipac::Paquet const &) = default;

unipac::Paquet & unipac::Paquet::operator=(unipac::Paquet &&) noexcept = default;

unipac::Paquet & unipac::Paquet::operator=(unipac::Paquet const &) = default;

unipac::Paquet::Paquet(unipac::paquet_id_t pid, universal::Version version) :
	_id(std::move(pid)),
	_version(std::move(version)) {
}

bool unipac::Paquet::operator==(Paquet const & p) const noexcept {
	return this->id() == p.id() && this->version() == p.version();
}

std::strong_ordering unipac::Paquet::operator<=>(unipac::Paquet const & p) const noexcept {
	auto const cmp_id = this->id() <=> p.id();
	if(cmp_id == std::strong_ordering::equal || cmp_id == std::strong_ordering::equivalent) {
		return this->version() <=> p.version();
	}
	return cmp_id;
} //// Paquet::operator<=>

std::strong_ordering unipac::Paquet::operator<=>(
	std::tuple<paquet_id_t, universal::Version> const & ids) const noexcept {
	auto const cmp_id = this->id() <=> std::get<0>(ids);
	if(cmp_id == std::strong_ordering::equal || cmp_id == std::strong_ordering::equivalent) {
		return this->version() <=> std::get<1>(ids);
	}
	return cmp_id;
} //// Paquet::operator<=>
