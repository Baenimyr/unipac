#include "universal/version.hpp"
#include <array>
#include <bit> // std::rotl
#include <cstddef> // std::size_t
#include <functional> // std::hash<>
#include <sstream>
#include <stdexcept> // std::invalid_argument

// -------------------- Version --------------------

namespace universal {

std::strong_ordering Version::operator<=>(Version const & v) const noexcept {
	std::strong_ordering comp = major <=> v.major;
	if(comp == std::strong_ordering::equal) {
		comp = minor <=> v.minor;
	}
	if(comp == std::strong_ordering::equal) {
		comp = patch <=> v.patch;
	}
	if(comp == std::strong_ordering::equal) {
		if(!release.has_value()) {
			if(v.release.has_value()) {
				return std::strong_ordering::greater;
			}
		} else if(!v.release.has_value()) {
			return std::strong_ordering::less;
		} else {
			comp = release.value() <=> v.release.value();
		}
	}
	if(comp == std::strong_ordering::equal) {
		if(!build.has_value()) {
			if(v.build.has_value()) {
				return std::strong_ordering::greater;
			}
		} else if(!v.build.has_value()) {
			return std::strong_ordering::less;
		} else {
			comp = build.value() <=> v.build.value();
		}
	}
	return comp;
}

Version::operator std::string() const noexcept {
	std::ostringstream s;
	s << *this;
	return s.str();
} //// Version::operator std::string()

std::ostream & operator<<(std::ostream & s, Version const & version) {
	s << version.major << '.' << version.minor;
	if(version.patch != 0) {
		s << '.' << version.patch;
	}
	if(version.release.has_value()) {
		s << '-' << version.release.value();
	}
	if(version.build.has_value()) {
		s << '+' << version.build.value();
	}
	return s;
} //// operator<<

Version to_version(std::string const & texte) {
	try {
		std::istringstream iss(texte);
		return to_version(iss);
	} catch(std::invalid_argument & ia) {
		throw std::invalid_argument("invalid_argunent: " + std::string(ia.what()) + " \"" + texte + "\"");
	}
} //// to_version

std::string _release(std::istream::int_type & c, std::string & stack, std::istream & texte) {
	while(true) {
		if(std::isalnum(c) || c == '.' || c == '-') {
			stack.push_back((char)c);
			c = texte.get();
		} else
			break;
	}

	std::string r = stack;
	stack.clear();
	return r;
} //// _release

Version to_version(std::istream & texte) {
	std::array<Version::version_t, 3> v = {0};
	decltype(v)::size_type i = 0;
	std::optional<std::string> release, build;

	std::string stack;
	auto c = texte.get();

	while(true) {
		switch(c) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				stack.push_back((char)c);
				c = texte.get();
				break;
			case '.':
				if(i >= 2)
					throw std::invalid_argument("Version has only MAJOR.MINOR.PATCH");
				v[i++] = std::stoul(stack);
				stack.clear();
				c = (char)texte.get();
				break;
			case std::char_traits<char>::eof():
				v[i++] = std::stoul(stack);
				goto end;
			case '-':
			case '+':
			default:
				v[i++] = std::stoul(stack);
				stack.clear();
				goto ext;
		}
	}

ext:
	while(true) {
		switch(c) {
			case std::char_traits<char>::eof():
				goto end;
			case '-':
				c = texte.get();
				release = _release(c, stack, texte);
				break;
			case '+':
				c = texte.get();
				build = _release(c, stack, texte);
				break;
			default:
				throw std::invalid_argument("Format incorrect");
		}
	}

end:
	Version ver(std::get<0>(v), std::get<1>(v), std::get<2>(v));
	ver.release = release;
	ver.build = build;
	return ver;
} //// to_version

// -------------------- VersionIntervalle --------------------

VersionIntervalle::VersionIntervalle(std::optional<Version> const & minimum, std::optional<Version> const & maximum,
	bool inclut_min, bool inclut_max) noexcept :
	minimum(minimum),
	maximum(maximum),
	inclut_min(inclut_min && minimum.has_value()),
	inclut_max(inclut_max && maximum.has_value()) {
}

VersionIntervalle::VersionIntervalle(
	std::optional<Version> && minimum, std::optional<Version> && maximum, bool inclut_min, bool inclut_max) noexcept :
	minimum(minimum),
	maximum(maximum),
	inclut_min(inclut_min && minimum.has_value()),
	inclut_max(inclut_max && maximum.has_value()) {
}

VersionIntervalle::operator std::string() const noexcept {
	std::ostringstream ss;
	ss << *this;
	return ss.str();
} //// VersionIntervalle::operator std::string

std::strong_ordering VersionIntervalle::operator<=>(VersionIntervalle const & i) const noexcept {
	std::strong_ordering cmp_minimum = minimum <=> i.minimum;
	if(cmp_minimum == std::strong_ordering::equal)
		cmp_minimum = this->inclut_min ? i.inclut_min ? std::strong_ordering::equal : std::strong_ordering::less
					  : i.inclut_min   ? std::strong_ordering::greater
									   : std::strong_ordering::equal;

	if(cmp_minimum == std::strong_ordering::equal) {
		std::strong_ordering cmp_maximum = maximum <=> i.maximum;
		if(cmp_maximum == std::strong_ordering::equal)
			cmp_maximum = this->inclut_max ? i.inclut_max ? std::strong_ordering::equal : std::strong_ordering::greater
						  : i.inclut_max   ? std::strong_ordering::less
										   : std::strong_ordering::equal;
		return cmp_maximum;
	} else {
		return cmp_minimum;
	}
} //// VersionIntervalle::operator<=>

VersionIntervalle & VersionIntervalle::operator&=(VersionIntervalle const & v2) {
	if(!minimum || (v2.minimum && v2.minimum > minimum)) {
		minimum = v2.minimum;
		inclut_min = v2.inclut_min;
	} else if(v2.minimum <=> minimum == std::strong_ordering::equivalent) {
		inclut_min &= v2.inclut_min;
	}

	if(!maximum || (v2.maximum && v2.maximum < maximum)) {
		maximum = v2.maximum;
		inclut_max = v2.inclut_max;
	} else if(v2.maximum <=> maximum == std::strong_ordering::equivalent) {
		inclut_max &= v2.inclut_max;
	}

	return *this;
} //// VersionIntervalle::operator&=

std::ostream & operator<<(std::ostream & os, VersionIntervalle const & v) {
	os << (v.inclut_min ? '[' : '(');
	if(v.minimum.has_value()) {
		os << v.minimum.value();
	}
	os << ',';
	if(v.maximum.has_value()) {
		os << v.maximum.value();
	}
	return os << (v.inclut_max ? ']' : ')');
} //// operator<<

VersionIntervalle to_version_intervalle(std::string_view texte) {
	if(texte[0] == '[' || texte[0] == '(') {
		if(texte[texte.size() - 1] != ']' && texte[texte.size() - 1] != ')') {
			throw std::invalid_argument(std::string(texte));
		}

		bool const inclut_min = texte[0] == '[';
		bool const inclut_max = texte[texte.size() - 1] == ']';

		auto sep = texte.find(',', 1);
		if(sep == texte.npos) {
			auto version = to_version(std::string(texte.substr(1, texte.size() - 2)));
			return {version, version, true, true};
		}

		std::optional<Version> version_min =
			(sep - 1) > 0 ? std::make_optional(to_version(std::string(texte.substr(1, sep - 1)))) : std::nullopt;
		++sep;
		while(sep < texte.size() && texte[sep] == ' ') {
			++sep;
		}
		std::optional<Version> version_max =
			(texte.size() - sep - 1) > 0
				? std::make_optional(to_version(std::string(texte.substr(sep, texte.size() - sep - 1))))
				: std::nullopt;

		if(texte[texte.size() - 1] != ']' && texte[texte.size() - 1] != ')') {
			throw std::invalid_argument(std::string(texte));
		}

		return {std::move(version_min), std::move(version_max), inclut_min, inclut_max};
	}

	size_t p = 0;
	VersionIntervalle v_inter(std::nullopt, std::nullopt, false, false);
	while(p < texte.size()) {
		auto const p2 = texte.find(',', p + 1);
		auto const sub = texte.substr(p, p2 - p);
		// TODO ajouter "~="
		if(sub.starts_with("==")) {
			auto v = to_version(std::string(sub.substr(2)));
			v_inter &= {v, v, true, true};
		} else if(sub.starts_with(">=")) {
			auto v = to_version(std::string(sub.substr(2)));
			v_inter &= {v, std::nullopt, true, false};
		} else if(sub.starts_with("<=")) {
			auto v = to_version(std::string(sub.substr(2)));
			v_inter &= {std::nullopt, v, false, true};
		} else if(sub.starts_with(">")) {
			auto v = to_version(std::string(sub.substr(1)));
			v_inter &= {v, std::nullopt, false, false};
		} else if(sub.starts_with("<")) {
			auto v = to_version(std::string(sub.substr(1)));
			v_inter &= {std::nullopt, v, false, false};
		} else {
			auto version_centre = to_version(std::string(sub));
			v_inter &= VersionIntervalle(version_centre, version_centre, true, true);
		}
		if(p2 == texte.npos)
			break;
		p = p2 + 1;
	}
	return v_inter;
} //// to_version_intervalle
} // namespace universal

std::size_t std::hash<universal::Version>::operator()(universal::Version const & v) const noexcept {
	std::size_t h = v.major;
	h ^= std::rotl(v.minor, 4);
	h ^= std::rotl(v.patch, 8);
	h ^= v.release.has_value() ? std::rotl(hash_string(v.release.value()), 12) : 0;
	h ^= v.build.has_value() ? std::rotl(hash_string(v.build.value()), 16) : 0;
	return h;
} //// std::hash<universal::Version>::operator()

std::size_t std::hash<std::optional<universal::Version>>::operator()(
	std::optional<universal::Version> const & version) const noexcept {
	return version.has_value() ? hash_version(version.value()) : 0;
} //// std::hash<std::optional<universal::Version>>::operator()

std::size_t std::hash<universal::VersionIntervalle>::operator()(
	universal::VersionIntervalle const & intervalle) const noexcept {
	std::size_t h = std::rotr(std::size_t(intervalle.inclut_min), 1) ^ std::rotr(std::size_t(intervalle.inclut_max), 2);
	h ^= hash_version(intervalle.minimum);
	h ^= hash_version(intervalle.maximum);
	return h;
} //// std::hash<universal::VersionIntervalle>::operator()
