#include <functional>
#include <gtest/gtest.h>
#include <optional>
#include <universal/version.hpp>

using namespace universal;

GTEST_TEST(Version, egal) {
	Version v(1, 13, 45);

	EXPECT_EQ(v, v);
	EXPECT_LT(v, Version(1, 13, 46));
	EXPECT_GT(v, Version(1, 13, 44));
};

GTEST_TEST(Version, format) {
	Version v(12, 45, 77);
	Version v2(14, 48, 7);
	Version v3(1, 78, 0);

	EXPECT_EQ((std::string)v, "12.45.77");
	EXPECT_EQ((std::string)v2, "14.48.7");
	EXPECT_EQ((std::string)v3, "1.78");
}

GTEST_TEST(Version, string) {
	Version v1 = to_version("1.78.00");
	Version v2 = to_version("1.14");
	Version v3 = to_version("14");

	EXPECT_EQ(v1, Version(1, 78, 0));
	EXPECT_EQ(v2, Version(1, 14, 0));
	EXPECT_EQ(v3, Version(14, 0));

	EXPECT_THROW(to_version("1.4..8"), std::invalid_argument);
	EXPECT_THROW(to_version("2.test"), std::invalid_argument);

	EXPECT_EQ(to_version((std::string)v1), v1);
}

GTEST_TEST(Version, intervalle_contiens) {
	VersionIntervalle inter(Version(1, 0, 0), Version(2, 0, 0), true, false);

	EXPECT_TRUE(inter.contains(Version(1, 0, 0)));
	EXPECT_TRUE(inter.contains(Version(1, 5845, 0)));
	EXPECT_TRUE(inter.contains(Version(1, 0, 127)));
	EXPECT_TRUE(!inter.contains(Version(2, 0, 0)));
	EXPECT_TRUE(!inter.contains(Version(2, 10, 0)));
	EXPECT_TRUE(!inter.contains(Version(0, 17, 0)));
};

GTEST_TEST(Version, intervalle_compare) {
	VersionIntervalle inter1(Version(1, 0), Version(1, 15), false, false);
	VersionIntervalle inter2(Version(1, 0), std::nullopt, true, false);
	VersionIntervalle inter3(Version(1, 1), std::nullopt, true, false);

	EXPECT_EQ(inter1, inter1);
	EXPECT_GT(inter1, inter2);
	EXPECT_LT(inter2, inter1);
	EXPECT_GT(inter3, inter1);
	EXPECT_LT(inter1, inter3);
	EXPECT_LT(inter2, inter3);
	EXPECT_GT(inter3, inter2);
}

GTEST_TEST(Version, intervalle_string) {
	EXPECT_EQ(to_version_intervalle("[1.13,1.13.2]"), VersionIntervalle(Version(1, 13), Version(1, 13, 2), true, true));
	EXPECT_EQ(to_version_intervalle("[1.13,1.13.2)"),
			  VersionIntervalle(Version(1, 13), Version(1, 13, 2), true, false));

	EXPECT_EQ(to_version_intervalle("1.7.10"), VersionIntervalle(Version(1, 7, 10), Version(1, 7, 10), true, true));
	EXPECT_THROW(to_version_intervalle("[1.38,1.39"), std::invalid_argument);
}

GTEST_TEST(Version, release) {
	Version v1(1, 15, 78);
	v1.release = "U5";

	EXPECT_EQ(((std::string)v1), "1.15.78-U5");

	Version v2 = to_version("1.16.2-U5");
	EXPECT_EQ(v2.major, 1);
	EXPECT_EQ(v2.minor, 16);
	EXPECT_EQ(v2.patch, 2);
	EXPECT_TRUE((v2.release.has_value() && v2.release.value() == "U5"));
	EXPECT_TRUE(!v2.build.has_value());
}

GTEST_TEST(Version, hash) {
	Version v1 (28, 48);
	Version v2 (486, 0, 17);
	VersionIntervalle vi1 (Version(48, 626), std::nullopt, true, false);
	VersionIntervalle vi2 (Version(0, 48, 78), Version(1, 58, 2), false, true);
	v2.release = "dev";

	std::hash<Version> h;
	std::hash<VersionIntervalle> hvi;
	EXPECT_EQ(h(v1), h(v1));
	EXPECT_EQ(h(v2), h(v2));
	EXPECT_EQ(hvi(vi1), hvi(vi1));
	EXPECT_EQ(hvi(vi2), hvi(vi2));
}
