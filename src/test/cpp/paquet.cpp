#include "unipac/paquet.hpp"
#include "pkgs.hpp"
#include "universal/version.hpp"
#include <gtest/gtest.h>

GTEST_TEST(Paquet, cmp) {
	auto const gcc = unipac::pkgs::gcc_11_2_0();
	auto const clang = unipac::pkgs::clang_14_0();

	EXPECT_EQ(gcc, gcc);
	EXPECT_LE(gcc, gcc);
	EXPECT_GE(gcc, gcc);
	EXPECT_TRUE((gcc <=> gcc) == 0);
	EXPECT_EQ(clang, clang);
	EXPECT_TRUE((clang <=> clang) == 0);
	EXPECT_NE(gcc, clang);
	EXPECT_GT(gcc, clang);
	EXPECT_GE(gcc, clang);
	EXPECT_LT(clang, gcc);
	EXPECT_LE(clang, gcc);

	unipac::Paquet const gcc10("gcc", universal::Version(10));
	EXPECT_NE(gcc, gcc10);
	EXPECT_LT(gcc10, gcc);
	EXPECT_GT(gcc, gcc10);
}
