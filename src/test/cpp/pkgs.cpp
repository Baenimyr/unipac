#include "pkgs.hpp"
#include "unipac/paquet.hpp"
#include "universal/version.hpp"
#include <optional>

namespace unipac::pkgs {

unipac::Paquet glibc_2_37() {
	Paquet glibc("glibc", universal::Version(2, 37));

	return glibc;
}

unipac::Paquet gcc_11_2_0() {
	unipac::Paquet gcc("gcc", universal::Version(11, 2, 0));
	gcc.arch = "amd64";
	gcc.desc = "This is the GNU C compiler, a fairly portable optimizing compiler for C.";
	gcc.os = "linux";
	gcc.url = "https://launchpad.net/ubuntu/jammy/+source/gcc-defaults";
	gcc.provisions.emplace("c-compiler", universal::VersionIntervalle::all());
	gcc.provisions.emplace("gcc-x86-64-linux-gnu", universal::VersionIntervalle::unique(universal::Version(11, 2, 0)));
	gcc.conflits.emplace(
		"gcc-doc", universal::VersionIntervalle(std::nullopt, universal::Version(2, 95, 3), false, false));

	return gcc;
}

Paquet clang_14_0() {
	Paquet clang("clang", universal::Version(14, 0));
	clang.arch = "amd64";
	clang.os = "linux";
	clang.provisions.emplace("c++-compiler");
	clang.provisions.emplace("c-compiler");
	clang.provisions.emplace("objc-compiler");
	clang.dependances.emplace("binutils");
	clang.dependances.emplace(
		"libc6", universal::VersionIntervalle(universal::Version(2, 34), std::nullopt, true, false));
	clang.dependances.emplace(
		"libstdc++6", universal::VersionIntervalle(universal::Version(11), std::nullopt, true, false));
	clang.dependances.emplace("libobjc-11-dev");

	return clang;
}

Paquet jre_headless_17_0_7() {
	Paquet jre_headless("jre-openjdk-headless", universal::Version(17, 0, 7));

	jre_headless.dependances.emplace("glibc", universal::VersionIntervalle::all());

	jre_headless.provisions.emplace(
		"java-runtime-headless", universal::VersionIntervalle::unique(universal::Version(17)));
	jre_headless.provisions.emplace(
		"java-runtime-headless-openjdk", universal::VersionIntervalle::unique(universal::Version(17)));
	return jre_headless;
}

Paquet jre_headless_11_0_19() {
	Paquet jre_headless("jre-openjdk-headless", universal::Version(11, 0, 19));

	jre_headless.dependances.emplace("glibc", universal::VersionIntervalle::all());

	jre_headless.provisions.emplace(
		"java-runtime-headless", universal::VersionIntervalle::unique(universal::Version(11)));
	jre_headless.provisions.emplace(
		"java-runtime-headless-openjdk", universal::VersionIntervalle::unique(universal::Version(11)));
	return jre_headless;
}

Paquet jre_headless_8_372() {
	Paquet jre_headless("jre-openjdk-headless", universal::Version(8, 372));

	jre_headless.provisions.emplace(
		"java-runtime-headless", universal::VersionIntervalle::unique(universal::Version(8)));
	jre_headless.provisions.emplace(
		"java-runtime-headless-openjdk", universal::VersionIntervalle::unique(universal::Version(8)));
	return jre_headless;
}

Paquet jre_17_0_7() {
	Paquet jre("jre-openjdk", universal::Version(17, 0, 7));

	jre.dependances.emplace("glibc", universal::VersionIntervalle::all());
	jre.dependances.emplace("jre-openjdk-headless", universal::VersionIntervalle::unique(jre.version()));

	jre.provisions.emplace("java-runtime", universal::VersionIntervalle::unique(universal::Version(17)));
	jre.provisions.emplace("java-runtime-openjdk", universal::VersionIntervalle::unique(universal::Version(17)));
	return jre;
}

Paquet jre_11_0_19() {
	Paquet jre("jre-openjdk", universal::Version(11, 0, 19));

	jre.dependances.emplace("glibc", universal::VersionIntervalle::all());
	jre.dependances.emplace("jre-openjdk-headless", universal::VersionIntervalle::unique(jre.version()));

	jre.provisions.emplace("java-runtime", universal::VersionIntervalle::unique(universal::Version(11)));
	jre.provisions.emplace("java-runtime-openjdk", universal::VersionIntervalle::unique(universal::Version(11)));
	return jre;
}

Paquet jre_8_372() {
	Paquet jre("jre-openjdk", universal::Version(8, 372));

	jre.dependances.emplace("jre-openjdk-headless", universal::VersionIntervalle::unique(jre.version()));

	jre.provisions.emplace("java-runtime", universal::VersionIntervalle::unique(universal::Version(8)));
	jre.provisions.emplace("java-runtime-openjdk", universal::VersionIntervalle::unique(universal::Version(8)));
	return jre;
}

} // namespace unipac::pkgs
