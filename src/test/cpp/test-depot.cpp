#include <gtest/gtest.h>
#include <unipac/depot.hpp>
#include <pkgs.hpp>

GTEST_TEST(Depot, ajout) {
	unipac::Depot depot;
	EXPECT_TRUE(depot.empty());

	auto const clang = unipac::pkgs::clang_14_0();
	depot.ajout(clang);
	EXPECT_FALSE(depot.empty());
	EXPECT_EQ(depot.size(), 1);

	depot.ajout(clang);
	EXPECT_EQ(depot.size(), 1);

	EXPECT_TRUE(depot.suppression(clang.id(), clang.version()));
	EXPECT_TRUE(depot.empty());
	EXPECT_EQ(depot.size(), 0);

    EXPECT_FALSE(depot.suppression(clang.id(), clang.version()));
    EXPECT_TRUE(depot.empty());
}

GTEST_TEST(Depot, contient) {
	unipac::Depot depot;

	depot.ajout(unipac::pkgs::jre_17_0_7());
	depot.ajout(unipac::pkgs::gcc_11_2_0());
	depot.ajout(unipac::pkgs::jre_11_0_19());
	depot.ajout(unipac::pkgs::jre_8_372());
	depot.ajout(unipac::pkgs::clang_14_0());

	{
		auto const gcc = unipac::pkgs::gcc_11_2_0();
		auto const jre = unipac::pkgs::jre_11_0_19();
		auto const jre_headless = unipac::pkgs::jre_headless_11_0_19();
		EXPECT_TRUE(depot.contient(gcc.id()));
		EXPECT_TRUE(depot.contient(gcc.id(), gcc.version()));
		EXPECT_TRUE(depot.contient(jre.id()));
		EXPECT_TRUE(depot.contient(jre.id(), jre.version()));
		EXPECT_FALSE(depot.contient(jre.id(), universal::Version(8, 371)));
		EXPECT_FALSE(depot.contient(jre_headless.id()));
	}

	{
		auto const gcc = unipac::pkgs::gcc_11_2_0();
		auto & gcc_ = depot.paquet(gcc.id(), gcc.version());
		EXPECT_EQ(gcc, gcc_);
	}
}

GTEST_TEST(Depot, final) {
	unipac::Depot depot;
	depot.ajout(unipac::pkgs::jre_headless_17_0_7());
	depot.ajout(unipac::pkgs::jre_headless_11_0_19());
	depot.ajout(unipac::pkgs::jre_headless_8_372());
	depot.ajout(unipac::pkgs::jre_17_0_7());
	depot.ajout(unipac::pkgs::jre_11_0_19());
	depot.ajout(unipac::pkgs::jre_8_372());

	EXPECT_EQ(depot.size(), 6);
}
