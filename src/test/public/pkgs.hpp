#pragma once

#include "unipac/paquet.hpp"

namespace unipac::pkgs {

Paquet glibc_2_37();

Paquet gcc_11_2_0();

Paquet clang_14_0();

Paquet jre_headless_17_0_7();
Paquet jre_headless_11_0_19();
Paquet jre_headless_8_372();
Paquet jre_17_0_7();
Paquet jre_11_0_19();
Paquet jre_8_372();

} // namespace unipac::pkgs
